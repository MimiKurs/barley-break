﻿var regTrigger = false;
var changeTrigger = true;
var relativeX;
var relativeY;
var GMaxHp=0;
var GCurrentHp=0;
var GMaxExp=0;
var GCurrentExp=0;
var GFreezeVar = 0;
var isFreezingMode = false;
var FreezeModeCheck = true;
function ExitScript()
{
    return location.href = "/";
}

function unloadBasd() {
   
}

function CookieEnter(Email,Pass)
{
    $(document).ready(function () {
        $("input[name='Email']").attr("value", Email);
        $("input[name='Password']").attr("value", Pass);
        $("input[type='submit']").click();
    });
}

function SetAvatarka(ImgName,Tmargin,Lmargin)
{
  //  alert(" Tmar -"+Tmargin+"  Lmar -"+Lmargin);
    $('#CabinetPhoto').html('&nbsp;<div id="ChangePhotoTrig">Download photo</div>');
    setOvotorka();
    $('#CabinetPhoto').css({"background-image":"url('ProfilePhotos/"+ImgName+"')","background-position":"-"+Lmargin+"px -"+Tmargin+"px","background-repeat":"no-repeat"});
    
}

function buildLastGameForm()
{
    $(document).ready(function () {
        $('#AjaxGetter').append('<form id="LoadLastGame" name="LoadLastGame" action="/Home/StartNewLastGame" data-ajax-confirm="Do you want continue Last Game?" data-ajax="true" data-ajax-mode="replace" data-ajax-update="#AjaxGetter" method="post"> <input type="submit" id="loadBTN"/> </form>');
        $('#loadBTN').click();
    });
}

function krestik() {
    changeTrigger = true;
    $("#NewGameSubmiti").removeAttr("disabled");
    $("#Bigblocku").remove();
  
}
   
function DownloadProfilePhotoToServer()
{
    $("#GameDesk").prepend("<div id='Bigblocku'> <span id='krestik' onclick='krestik()'>&nbsp; x &nbsp;</span> <form id='form0' method='post' enctype='multipart/form-data' data-ajax-update='#AjaxGetter' data-ajax-mode='replace' data-ajax='true' action='/Home/DownloadPhotoToServer'><input type='file' name='fileUpload'><br><input id='SubmitSingle' type='submit' value='Upload' name='Submit'></form>  </div>");
}


function ChangeScript(variable)
{
    if (changeTrigger) {
        $("#NewGameSubmiti").attr("disabled","disabled");
        if (variable != null) {
          
            if (variable == "Name") {
                $("#GameDesk").prepend("<div id='Bigblocku'> <span id='krestik' onclick='krestik()'>&nbsp; x &nbsp;</span> <form id='centerForm' name='centerForm' action='/Home/ChangeValue' data-ajax-confirm='Do you want to change your name?' data-ajax='true' data-ajax-mode='replace' data-ajax-update='#AjaxGetter' method='post'> <input type='text' value='New Name' name='NewValue'><input type='text' value='Name' name='variable'style='display:none;' required> <input type='submit' value='Confirm'></form>  </div>");
               
            }
            else if (variable =="Surname") {
                $("#GameDesk").prepend("<div id='Bigblocku'> <span id='krestik' onclick='krestik()'>&nbsp; x &nbsp;</span> <form id='centerForm' name='centerForm' action='/Home/ChangeValue' data-ajax-confirm='Do you want to change your Surname?' data-ajax='true' data-ajax-mode='replace' data-ajax-update='#AjaxGetter' method='post'> <input type='text' value='New Surname' name='NewValue'><input type='text' value='Surname' name='variable'style='display:none;' required> <input type='submit' value='Confirm'></form>  </div>");
               
            }
            else if (variable == "Pass") {
                $("#GameDesk").prepend("<div id='Bigblocku'> <span id='krestik' onclick='krestik()'>&nbsp; x &nbsp;</span> <form id='centerForm' name='centerForm' action='/Home/ChangeValue' data-ajax-confirm='Do you want to change your Password?' data-ajax='true' data-ajax-mode='replace' data-ajax-update='#AjaxGetter' method='post'> <b>Old password</b> <input type='password' name='oldPass' class='changePass' ><b>New password</b>  <input type='password' name='NewValue' class='changePass' >  <input type='text' value='Pass' name='variable'style='display:none;' required> <input type='submit' value='Confirm'></form>  </div>");
            }
            else if (variable == "Games") {
                $("#GameDesk").prepend("<div id='Bigblocku'> <span id='krestik' onclick='krestik()'>&nbsp; x &nbsp;</span> <form id='centerForm' name='centerForm' action='/Home/ChangeValue' data-ajax-confirm='Do you want to clear all statistics?' data-ajax='true' data-ajax-mode='replace' data-ajax-update='#AjaxGetter' method='post'> <input type='text' value='Games' name='variable'style='display:none;' required> <input type='submit' value='Confirm erasing'></form>  </div>");
            }
        }
        else
            ProcessedToCabinet(false);
        changeTrigger = false;
    }
}

    function ProcessedToCabinet(variable,text) {
        if (variable) {
            if (text == undefined) {
           
                $('#InformBlock').text('processed to cabinet');
                $('#InformBlock').css('border', '1px dotted green');
                $('#InformBlock').animate({ opacity: 1 }, 1000);
                $('#InformBlock').animate({ opacity: 0 }, 2000);
            }
            else {
                $('#InformBlock').text('');
                $('#InformBlock').append(text);
                $('#InformBlock').css('border', '1px dotted green');
                $('#InformBlock').animate({ opacity: 1 }, 1000);
                $('#InformBlock').animate({ opacity: 0 }, 2000);

            }

        }
        else {
            if (text == undefined) {
                if ($('#InformBlock').css('opacity') != 0)
                    $('#InformBlock').animate({ opacity: 0 }, 200);
                $('#InformBlock').text('An error occured while process to cabinet. Chech data and retry.');
          
                
            }
            else {
                if ($('#InformBlock').css('opacity') != 0)
                    $('#InformBlock').animate({ opacity: 0 }, 200);
                $('#InformBlock').text('');
                $('#InformBlock').append(text);
            }
       
      
            $('#InformBlock').css({ 'border': '1px dotted red'});
            $('#InformBlock').animate({ opacity: 1 }, 1000);

        }

    };

    function setOvotorka()
    {
        $("#ChangePhotoTrig").click(function () {
        
            if (changeTrigger) {
                //    $("#GameDesk").prepend("<div id='Bigblocku'> <span id='krestik' onclick='krestik()'>&nbsp; x &nbsp;</span> <form id='centerForm' method='post' enctype='multipart/form-data' data-ajax-update='#AjaxGetter' data-ajax-mode='replace' data-ajax='true' action='/Home/downloadFile'> <input type='file' name='fileUpload'><br><input id='SubmitSingle' type='submit' value='Upload' name='Submit'></form>  </div>");
                // $("#GameDesk").prepend('<form id="form0"  enctype="multipart/form-data" data-ajax-update="#AjaxGetter" data-ajax-mode="replace" data-ajax="true" action="/Home/downloadFile"> <input type="file" name="fileUpload"><br><input id="SubmitSingle" type="submit" value="Upload" name="Submit"></form>');
                //  $("#GameDesk").prepend('<form id="form0"  enctype="multipart/form-data" data-ajax-update="#AjaxGetter" data-ajax-mode="replace" data-ajax="true" action="/Home/downloadFile"> <input type="file" name="fileUpload"><br><input id="SubmitSingle" type="submit" value="Upload" name="Submit"></form>');
                $("#GameDesk").prepend("<div id='Bigblocku'> <span id='krestik' onclick='krestik()'>&nbsp; x &nbsp;</span> <span id='ZcP'> </span><div id='CPdanger'> Выделенная область будет сохранена как фото профиля. Просто выберете область и нажмите. </div> <div id='CPicture'><img style='z-index:0;' src='ProfilePhotos/1111.jpg'>  </div><form name='SavePicStatus' action='/Home/downloadFile' data-ajax-confirm='Do you want accept new Picture?' data-ajax='true' data-ajax-mode='replace' data-ajax-update='#AjaxGetter' method='post' name='sadas'> <input id='CPDone' type='submit' value='Done' disabled> <input type='text' name='Tmargin' style='display:none;'><input type='text' name='Lmargin' style='display:none;'>  <input id='CPAgain'  type='button' value='Try again'/> </form> </div>");
                changeTrigger = false;

                $("#CPAgain").click(function () {
                    $("#CPicture").die();
                    $("#CPicture").css("cursor", "none");
                    $("#CPicture").live("mousemove", function (e) {

                        var offset = $("#CPicture").offset();

                        relativeX = (e.pageX - offset.left);
                        relativeY = (e.pageY - offset.top);

                        // $("#ZcP").html('X=' + relativeX + '<br/>' + 'Y=' + relativeY);
                        $("#CPicture").html('<div id="innerCabinetPhoto">&nbsp; </div>' + '<img src="ProfilePhotos/1111.jpg">');



                        if (relativeX < 59 || relativeX > 539) {
                            if (relativeX < 59)
                                relativeX = 59;
                            if (relativeX > 539)
                                relativeX = 539;
                        }
                        if (relativeY < 93 || relativeY > 324) {
                            if (relativeY < 93)
                                relativeY = 93;
                            if (relativeY > 324)
                                relativeY = 324;
                        }

                       

                        $("#CPDone").attr("disabled", "true");
                        $("#innerCabinetPhoto").css("margin-top", relativeY - 95);
                        $("#innerCabinetPhoto").css("margin-left", relativeX - 60);


                    });
                });

                $("#CPicture").live("mousemove", function (e) {

                    var offset = $("#CPicture").offset();

                    relativeX = (e.pageX - offset.left);
                    relativeY = (e.pageY - offset.top);

                    // $("#ZcP").html('X=' + relativeX + '<br/>' + 'Y=' + relativeY);
                    $("#CPicture").html('<div id="innerCabinetPhoto">&nbsp; </div>' + '<img src="ProfilePhotos/1111.jpg">');

                    if (relativeX < 59 || relativeX > 539) {
                        if (relativeX < 59)
                            relativeX = 59;
                        if (relativeX > 539)
                            relativeX = 539;
                    }
                    if (relativeY < 93 || relativeY > 324) {
                        if (relativeY < 93)
                            relativeY = 93;
                        if (relativeY > 324)
                            relativeY = 324;
                    }


                    

                    $("#innerCabinetPhoto").css("margin-top", relativeY - 95);
                    $("#innerCabinetPhoto").css("margin-left", relativeX - 60);


                });


                $("#CPicture").click(function () {
                    $("#CPicture").css("cursor", "default");
                 //   alert(" Tmar -" + relativeY + "  Lmar -" + relativeX);
                    $('input[name="Tmargin"]').attr("value", relativeY - 93);
                    $('input[name="Lmargin"]').attr("value", relativeX - 59);
                    $("#CPicture").attr("cursor", "default");
                    $("#CPDone").removeAttr("disabled");
                    $("#CPicture").die();
                    $("#innerCabinetPhoto").css("background-color", "rgba(255, 86, 0, 0.56)");
                });

            }

        });
    }

    function HelpHoverBar(Picture, toblock) {
        if (toblock) {
            if (Picture) {
               // alert(BlockPictureName);
                //alert(PicWay + BlockPictureName + ".jpg");
                $("#GameDesk").prepend("<div id='Bigblocku22' style='background-image:url(\"" + PicWay + BlockPictureName + ".jpg\")' > &nbsp; </div>");
            }
            else {
                $("#GameDesk").prepend("<div id='Bigblocku22' style='font-size:30px;text-align:center;padding-top:250px; background-color:white;height:350px;'> Help Avaible only for game-picture mode </div>");
            }
        }
        else {
            
            $("#Bigblocku22").remove();
        }
    }
    function SetHpBarTo(MaxHp, CurrentHp) {
        GMaxHp = MaxHp;
        GCurrentHp = CurrentHp;
        var HpBarRes = "";
        ProcHp = ((CurrentHp * 100) / MaxHp);

        HpBarRes = "<div id='SelfHPBar'> <div id='NumbersHE' > " + CurrentHp + "/" + MaxHp + " </div> <div id='HaveHpBar' style='width:" + ProcHp + "%;'>&nbsp;</div> <div id='MissHpBar' style='width:" + (100 - ProcHp) + "%;'>&nbsp;</div></div>";
        $("#HpBar").html("Hp " + HpBarRes);

    }
    function SetExpBarTo(MaxExp, CurrentExp) {
        var ExpBarRes = "";
        ProcHp = ((CurrentExp * 100) / MaxExp);

        ExpBarRes = "<div id='SelfHPBar'> <div id='NumbersXE' > " + CurrentExp + "/" + MaxExp + " </div> <div id='HaveExpBar' style='width:" + ProcHp + "%;'>&nbsp;</div> <div id='MissExpBar' style='width:" + (100 - ProcHp) + "%;'>&nbsp;</div></div>";
        $("#ExpBar").html("Exp " + ExpBarRes);
        GMaxExp = MaxExp;
        GCurrentExp = CurrentExp;
    }

    function DegradeHpBarTo(MissedHp) {
        var ProcHp = (((GCurrentHp - MissedHp) * 100) / GMaxHp);
        $('#NumbersHE').html((GCurrentHp - MissedHp) + '/' + GMaxHp);
        $('#HaveHpBar').animate({ "width": ProcHp + "%" }, 1000);
        $('#MissHpBar').animate({ "width": (100 - ProcHp) + "%" }, 1000);
    }


    function DegradeExpBarTo(isLevel, NewVaueOrAddExp) {
        /*
        if (searchAndCheck.CurrentExp + RecivedExp < searchAndCheck.MaxExp)
    searchAndCheck.CurrentExp += RecivedExp;
else
{
    while ((RecivedExp - (searchAndCheck.MaxExp + searchAndCheck.CurrentExp)) > 0)
    {
        searchAndCheck.Lvl++;
        searchAndCheck.MaxExp = searchAndCheck.MaxExp + (searchAndCheck.MaxExp / 100 * 50);
    }
    if (RecivedExp <= 0)
        searchAndCheck.CurrentExp = 0;
    else
    searchAndCheck.CurrentExp = RecivedExp;

    
}
        
        var OldCurrentExp = GCurrentExp;
        var CurMaxExp = GMaxExp;

        if (OldCurrentExp + RecivedExp < CurMaxExp) {
            var ProcExp = (((RecivedExp + OldCurrentExp) * 100) / GMaxExp);
            $('#NumbersXE').html((RecivedExp + OldCurrentExp) + '/' + CurMaxExp);
            $('#HaveExpBar').animate({ "width": ProcExp + "%" }, 1000);
            $('#MissExpBar').animate({ "width": (100 - ProcExp) + "%" }, 1000);
            GCurrentExp= RecivedExp + OldCurrentExp
        }
        else {
            while ((RecivedExp - (CurMaxExp - OldCurrentExp)) > 0) {
                //span #SpanLevelChange
                OldLevel = OldLevel + 1;
                $('#SpanLevelChange').css({ "font-size": "50px", "font-weight": "900" });
                $('#SpanLevelChange').html(OldLevel);
                $('#SpanLevelChange').animate({ "font-size": "16px" }, 1000);
                setTimeout(function () { $('#SpanLevelChange').css({ "font-weight": "100" }); }, 1000);
                CurMaxExp = CurMaxExp + (CurMaxExp / 100 * 50);

                var ProcExp = (((OldCurrentExp) * 100) / CurMaxExp);
                $('#NumbersXE').html((OldCurrentExp) + '/' + CurMaxExp);
                $('#HaveExpBar').animate({ "width": ProcExp + "%" }, 1000);
                $('#MissExpBar').animate({ "width": (100 - ProcExp) + "%" }, 1000);

            }

        }*/
        
        if (isLevel) {
           // $('#NumbersXE').html(parseInt(GMaxExp) + '/' +parseInt( GMaxExp));
            $('#HaveExpBar').animate({ "width": "100%" }, 1000);
            $('#MissExpBar').animate({ "width": "0%" }, 1000);
            
            setTimeout(function () {
                GMaxExp = GMaxExp / 100 * 150;
                $('#NumbersXE').html(0 + '/' +parseInt( GMaxExp));
                $('#HaveExpBar').css({ "width": "0%" });
                $('#MissExpBar').css({ "width": "100%" });

                var ProcExp = (((NewVaueOrAddExp) * 100) / GMaxExp);
                $('#NumbersXE').html(parseInt(NewVaueOrAddExp) + '/' + parseInt(GMaxExp));
                $('#HaveExpBar').animate({ "width": ProcExp + "%" }, 1000);
                $('#MissExpBar').animate({ "width": (100 - ProcExp) + "%" }, 1000);
            }, 1500);
            var OldLevel = parseInt($('#SpanLevelChange').html());
            $('#SpanLevelChange').css({ "font-size": "50px", "font-weight": "900" });
            $('#SpanLevelChange').html(OldLevel);
            $('#SpanLevelChange').animate({ "font-size": "13.6px" }, 1000);
            setTimeout(function () { $('#SpanLevelChange').css({ "font-weight": "400" }); }, 1700);
        }
        else {
            
            var ProcExp = (((NewVaueOrAddExp) * 100) / GMaxExp);
            $('#NumbersXE').html((NewVaueOrAddExp) + '/' + GMaxExp);
            $('#HaveExpBar').animate({ "width": ProcExp + "%" }, 1000);
            $('#MissExpBar').animate({ "width": (100 - ProcExp) + "%" }, 1000);

        }
       

    }
    function GetTopReq()
    {
        if (changeTrigger) {
            $("#getTopTBS").click();
        }
        changeTrigger = false;
    }

    function TransformToCabinet(variable,Level, Name, Surname, Email, CompleteGames, IncompleteGames, StepsInGame, MaxHp, MaxExp, CurrentExp)
    {
       // var MaxHp = 100;
        var CurrentHp = 100;
       // var MaxExp = 500;
       // var CurrentExp = 123;

        if (variable) {
            CompleteGames = parseInt(CompleteGames);

            IncompleteGames = parseInt(IncompleteGames);
            StepsInGame = parseInt(StepsInGame);

            //transform login bar to cabinet information
            $(document).ready(function () {
                var a = parseInt(StepsInGame / CompleteGames);
                if (isNaN(a))
                    a = 0;
                $("#LinkAndLogin").css({ margin: 0, height: "100%", width: 250, opacity: "0" }, 1000).text("").css({ "border": "none", "float": "left " })
                .append('<div id="CabinetPhoto"> Your Photo Here<div id="ChangePhotoTrig">Download photo</div></div> <div id="PersonalBaseInfo" > <b>Name: </b><br/> ' + Name + '<br/><b> Surname: </b><br/> ' + Surname + '<br/><b>Email: </b><br/> ' + Email + '<br/>  </div>')
                 .append(' <div id="HpExpBar"><div id="HpBar"></div><div id="ExpBar"></div></div> ')
                 .append('<div id="Statistics" > <b> Statistics </b> <div>')
                .append('<div id="StatisticsInfo">Level: <span id="SpanLevelChange">' + Level + '</span> <br/> Games played:  ' + (CompleteGames + IncompleteGames) + ' <br/>Complete Games: ' + CompleteGames + ' <br/> Incomplete Games :  ' + IncompleteGames + ' <br/> Steps In Game:  ' + StepsInGame + ' <br/> Average Steps:  ' + a + ' <br/></div>')
                .append('<div id="Settings"><b> Settings </b> </div>')
                .append('<div id="SettingsInfo"> <a style="cursor:pointer;" onclick="ChangeScript(\'Name\')"> Change First Name</a> <br/><a style="cursor:pointer;" onclick="ChangeScript(\'Surname\')"> Change Last Name</a> <br/><a style="cursor:pointer;" onclick="ChangeScript(\'Pass\')"> Change Password</a> <br/><a style="cursor:pointer;" onclick="ChangeScript(\'Games\')"> Clear Games Statistics</a> <br/><a id="DownloadphotoA" style="cursor:pointer;" onclick="DownloadProfilePhotoToServer()"> Download photo to server </a> <form  name="ConfigGame" action="/Home/RequestTopUsers" data-ajax="true"  data-ajax-mode="replace" data-ajax-update="#AjaxGetter" method="post"> <a style="cursor:pointer;" onclick="GetTopReq()"> Top users </a> <input id="getTopTBS" style="display:none;" type="submit" value="Top users" /> </form> </div>')
                .append('<div id="Exit"> <a onclick="ExitScript();"><b>  Exit</b>  </a> </div>');
                $("section:eq(1)").append('<div id="GameBox" > </div>');
                $("#GameBox").append('<div id="GameDesk" > </div>');
                $("#GameBox").append('<div id="GameStatus"> <b> Game Status: </b> </div>');
                $("#GameBox").append('<div id="GameHelp"> help?  </div>');
                $("#GameStatus").append(' <form id="ConfigGame" name="ConfigGame" action="/Home/StartNewGame" data-ajax="true" data-ajax-confirm="Do you want to start new game?" data-ajax-mode="replace" data-ajax-update="#AjaxGetter" method="post"> <input id="NewGameSubmiti" type="submit" value="New Game" /> <input type="button" value="&nbsp;" id="ConfigBtnGS" title="Config" onclick="ConfigGameButton()"> </form>');
                $("#GameStatus").append(' <form id="SaveGameStatus" name="SaveGameStatus" action="/Home/SaveGameStatus" data-ajax-confirm="Do you want to save Status?" data-ajax="true" data-ajax-mode="replace" data-ajax-update="#AjaxGetter" method="post"> <input type="submit" value="Save Game Status" disabled id="saveStatusz"  /><div style="display:none;"> <input type="text" name="StepsNumber"><input type="text" name="Mas0"> <input type="text" name="Mas1"> <input type="text" name="Mas2"><input type="text" name="Mas3"><input type="text" name="Mas4"><input type="text" name="Mas5"><input type="text" name="Mas6"><input type="text" name="Mas7"><input type="text" name="Mas8"><input type="text" name="Mas9"><input type="text" name="Mas10"><input type="text" name="Mas11"><input type="text" name="Mas12"><input type="text" name="Mas13"><input type="text" name="Mas14"><input type="text" name="Mas15"> <input type="text" name="bPicName"> <input type="text" name="isPicThere"><input type="submit" name="EndGameSubmit"></div> </form>');
                $("#GameStatus").append('<b>Number of moves:</b> <div id="NumberSteps"> 0 </div>')

                $("#LinkAndLogin").css("background-color", "rgb(245, 245, 245)");
               
              //  $("body").attr("onunload", "unloadBasd()");

                setTimeout(function () {
                    $("#LinkAndLogin").animate({ "opacity": "1" });


                }, 100);
                setTimeout(function () {

                    $("#GameDesk").animate({ "opacity": "1" });


                }, 600);
                setTimeout(function () {

                    $("#GameStatus").animate({ "opacity": "1" });

                }, 400);


              

                //----------------hp exp bar set
                SetExpBarTo(MaxExp, CurrentExp);
                SetHpBarTo(MaxHp, CurrentHp);
                // DegradeHpBarTo(50);
                // DegradeExpBarTo(false,100);
                //-0-------------


                var StatistBool = false;
                var SettingBool = false;
                $("#Statistics").click(function () {
                    if (!StatistBool) {
                        $("#StatisticsInfo").slideUp();
                    }
                    else {
                        $("#StatisticsInfo").slideDown();

                    }
                    StatistBool = !StatistBool;
                });
                $("#Settings").click(function () {
                    if (!SettingBool) {
                        $("#SettingsInfo").slideUp();
                    }
                    else {
                        $("#SettingsInfo").slideDown();
                    }
                    SettingBool = !SettingBool;
                });

                $("#CabinetPhoto").hover(function () {
                    setTimeout(function () {
                        $("#ChangePhotoTrig").css("opacity", "1");
                        //  $("#ChangePhotoTrig").animate({"height":"35px","margin-top":"100px"},300);
                    }, 300);
                }, function () {
                    setTimeout(function () {
                        // $("#ChangePhotoTrig").animate({ "height": "0px", "margin-top": "135px" }, 300);
                        $("#ChangePhotoTrig").css("opacity", "0");
                    }, 300);

                });

                var browser = navigator.appName;
              //  alert (browser == "Microsoft Internet Explorer")
                if (browser == "Microsoft Internet Explorer") {
                    $("#GameBox").css("margin-top", "-505px");
                    $("#GameDesk").css("margin-top", "-221px");
                    $("#GameHelp").css("margin-top", "-271px");
                }
                setOvotorka();

                $("#GameHelp").hover(function () { HelpHoverBar(BlockHavePicture, true); }, function () { HelpHoverBar(BlockHavePicture, false); });


            });
        }
        else {
            //transform cabinet information to login bar
            var sdifs;
        }
            
    }
   
    function StartFury()
    {
        
        // FreezeModeCheck
        if (FreezeModeCheck)
        {
            freezing();
        }
        FreezeModeCheck = true;
    }

    function freezing()
    {
        
       
        $("body").prepend("<img class='freezingImg' src='/content/2nd_stage.png'  height='" + $('html').height() + "px' width='100%' style='position:absolute; opacity:0;'/>");
        $("body").prepend("<img class='freezingImg' src='/content/2nd_stage.png'  height='99%' width='100%' style='position:absolute; transform:rotate(180deg);  opacity:0;'/>");
        $(".freezingImg").animate({ "opacity": "1" }, 1000);
        $(".freezingImg").animate({ "opacity": "0" }, 1700);
        setTimeout(function () { $(".freezingImg").remove() }, 2700);
        GFreezeVar += parseInt( Math.random() * (30 - 1) + 1);

       
        if (parseInt(GFreezeVar) >= 100) {
            DegradeHpBarTo(100);
            isFreezingMode = false;
            //    $("#GameDesk").html("<form style='display:none;'> <input type='submit' id='failedGameSubmit'><form>");
            $("#GameDesk").html("You can try again if want.");
            EndGameIntervalClearer();
            
            ZintervalID = null;
            alert('You died!!!!!!');
            

        }
        else {
            DegradeHpBarTo(GFreezeVar);
           // alert(GFreezeVar);
        }
    }


    $(document).ready(function () {

      
       


        $("#LinkBarReg").click(function () {
       
            if (regTrigger == false) {
                //Registration
                $("#LinkBarReg").hover(function () {
                $("#LinkBarReg").css("background-color", "rgba(57, 0, 255, 0.42)");
                }, function () {
                $("#LinkBarReg").css("background-color", "rgba(206, 0, 255, 0.45)");
                });
                $("#LinkBarReg").text("Log in");
                $("#LoginBar").animate({ height: "+=240" }, 500);
                $("#ifRegistrate").slideDown();
                $("form[name='LoginForm']").attr('action', '/Home/Registrate');
                $("#NewSubmit1").css('visibility', 'hidden');
                $(":submit").attr('value', 'Register');
                $("NewSubmit2").css('display', 'none');
                
                $("#BRemember").css('visibility', 'hidden');
          
            
                regTrigger = true;
            }
            else
            {
                //LogIn
                $("#LinkBarReg").hover(function () {
                    $("#LinkBarReg").css("background-color", "rgba(255, 226, 0, 0.72)");
                }, function () {
                    $("#LinkBarReg").css("background-color", "rgba(255, 164, 0, 0.67)");
                });
                $("#LinkBarReg").text("Registrate");
                $("#LoginBar").animate({ height: "-=240" }, 500);
                $("#ifRegistrate").slideUp();
                $("form[name='LoginForm']").attr('action', '/Home/LogOn');
                $(":submit").attr('value', 'Enter');
                $("#NewSubmit1").css('display', 'block');
                $("#NewSubmit1").css('visibility', 'visible');
                $("#BRemember").css('visibility', 'visible');
                regTrigger = false;
            }

          

       

        });

 
   
    });

