﻿var Coord = new Array(32);
var BlockInMassive = new Array(16);
var StepsInGama = 0;
var FirstDisable = true;
var isGameContinued = false;
var body = "Body";
var PicWay = "/Content/BlockImages/";
var BlockHavePicture = false;
var BlockPictureName = "";
var ZintervalID;
//left
Coord[0] = "0px";
Coord[1] = "150px"; Coord[2] = "300px"; Coord[3] = "450px";
Coord[4] = Coord[0]; Coord[5] = Coord[1]; Coord[6] = Coord[2];
Coord[7] = Coord[3]; Coord[8] = Coord[0]; Coord[9] = Coord[1];
Coord[10] = Coord[2]; Coord[11] = Coord[3]; Coord[12] = Coord[0];
Coord[13] = Coord[1]; Coord[14] = Coord[2];
Coord[15] = Coord[3];
//top
Coord[16] = "0px"; Coord[17] = Coord[16]; Coord[18] = Coord[16];
Coord[19] = Coord[16]; Coord[20] = "150px"; Coord[21] = Coord[20];
Coord[22] = Coord[20]; Coord[23] = Coord[20]; Coord[24] = "300px";
Coord[25] = Coord[24]; Coord[26] = Coord[24]; Coord[27] = Coord[24];
Coord[28] = "450px"; Coord[29] = Coord[28]; Coord[30] = Coord[28];
Coord[31] = Coord[28];

function blockMoveControl() {
    $(document).ready(function () {

      

    $(".Block").click(function () {
    
            var Number = BlockInMassive.indexOf($(this).attr("id"));
            //alert(Number);

            var CanMoveTo = -1;
            // alert(Number);i
            //to left
            if (Number != 0 || Number != 4 || Number != 8 || Number != 12) {
                if (BlockInMassive[Number - 1] == "")
                    CanMoveTo = Number - 1;
            }
            //to right
            if (Number != 3 || Number != 7 || Number != 11 || Number != 15) {
                if (BlockInMassive[Number + 1] == "")
                    CanMoveTo = Number + 1;
            }
            //to top
            if (Number != 0 || Number != 1 || Number != 2 || Number != 3) {
                if (BlockInMassive[Number - 4] == "")
                    CanMoveTo = Number - 4;
            }
            //to bot
            if (Number != 12 || Number != 13 || Number != 14 || Number != 15) {
                if (BlockInMassive[Number + 4] == "")
                    CanMoveTo = Number + 4;
            }

            if (CanMoveTo == -1) {
                //  alert("Cant move");
            }
            else {
                // alert("start movin'");
                BlockInMassive[CanMoveTo] = BlockInMassive[Number];
                BlockInMassive[Number] = "";

                $("#" + BlockInMassive[CanMoveTo]).animate({ "margin-left": Coord[CanMoveTo], "margin-top": Coord[CanMoveTo + 16] });
                CheckMassiv();
                StepsInGama++;
                $('#NumberSteps').text(StepsInGama);
                $('input[name="StepsNumber"]').attr("value", StepsInGama);

               $('input[name="Mas0"]').attr("value", BlockInMassive[0]);
                $('input[name="Mas1"]').attr("value", BlockInMassive[1]);
                $('input[name="Mas2"]').attr("value", BlockInMassive[2]);
                $('input[name="Mas3"]').attr("value", BlockInMassive[3]);
                $('input[name="Mas4"]').attr("value", BlockInMassive[4]);
                $('input[name="Mas5"]').attr("value", BlockInMassive[5]);
                $('input[name="Mas6"]').attr("value", BlockInMassive[6]);
                $('input[name="Mas7"]').attr("value", BlockInMassive[7]);
                $('input[name="Mas8"]').attr("value", BlockInMassive[8]);
                $('input[name="Mas9"]').attr("value", BlockInMassive[9]);
                $('input[name="Mas10"]').attr("value", BlockInMassive[10]);
                $('input[name="Mas11"]').attr("value", BlockInMassive[11]);
                $('input[name="Mas12"]').attr("value", BlockInMassive[12]);
                $('input[name="Mas13"]').attr("value", BlockInMassive[13]);
                $('input[name="Mas14"]').attr("value", BlockInMassive[14]);
                $('input[name="Mas15"]').attr("value", BlockInMassive[15]);
              
              
                if (FirstDisable) {
                    $('#saveStatusz').removeAttr('disabled');
                    $("#SaveGameStatus").attr("data-ajax-confirm", "Do you want to save Status?");
                 

                    FirstDisable = false;
                }
                if (isFreezingMode) {
                    $('#saveStatusz').attr('disabled', 'disabled');
                    FreezeModeCheck = false;
                }

               
               

            }



        });
    });
}


function MassiveToNormalForm() {
    for (var i = 0; i < 15; i++) {
        BlockInMassive[i] = "Block" + i;
    }
    BlockInMassive[15] = "";
}

function closeLikeKrestik()
{
    if ($("#chkBoxForFreezingMode").attr("checked") == "checked") {
       // alert(isFreezingMode);
        isFreezingMode = true;
    }
    
    if ($("#chkBlockNumbers").attr("checked") == "checked") {
        BlockHavePicture = false;
    }
    else {
        BlockHavePicture = true;
        BlockPictureName = $("#HiddenInputPicName").attr("value");
    }
  //  alert(BlockHavePicture + "   f   " + BlockPictureName);
    krestik();
}

function ConfigGameButton()
{
    if (changeTrigger) {
        $("#NewGameSubmiti").attr("disabled", "disabled");
        var picsTable = "<table id='picsTable'><tr> <td> first </td> </tr> <tr> <td> second </td> </tr><tr> <td> third </td> </tr></table>";
        var innerText = "<input name='hideMe' type='radio' id='chkBlockNumbers' style='position:absolute; width:50px; margin-left:100px;'/>Blocks- Numbers <br/><input type='radio' name='hideMe' id='chkBlockPictures' style='position:absolute; width:50px;margin-left:100px;'/> Blocks- Pictures<br/> <div id='BlockChoosePicture'>" + picsTable + " </div><input type='checkbox' id='chkBoxForFreezingMode'/> Enable freezing-mode game <br/> <input id='acceptBlockStyle' type='button' value='apply' onclick='closeLikeKrestik()' disabled/> <input type='text' id='HiddenInputPicName' style='display:none'> ";
        $("#GameDesk").prepend("<div id='Bigblocku'> <span id='krestik' onclick='krestik()' style='margin:0; margin-left:124px;margin-top:5px;'>&nbsp; x &nbsp;</span><div id='WarningCBlock'> Config will accept after start new game</div> <form id='centerForm' name='centerForm' action='/Home/ChangeValue' data-ajax-confirm='Do you want to change your name?' data-ajax='true' data-ajax-mode='replace' data-ajax-update='#AjaxGetter' method='post'> " + innerText + "</form>  </div>");
      
        $("#chkBlockNumbers").click(function () {
          //  $("#chkBlockPictures").removeAttr("checked");
            $("#acceptBlockStyle").removeAttr("disabled");
            $("#BlockChoosePicture").css("display", "none");
        });
        $("#chkBlockPictures").click(function () {
            $("#acceptBlockStyle").attr("disabled", "disabled");
          //  $("#chkBlockNumbers").removeAttr("checked");
            $("#BlockChoosePicture").css("display", "block");

        });

        $("#picsTable td").click(function () {
            $("#acceptBlockStyle").removeAttr("disabled");
            $("#picsTable td").css("background-color", "white");
            $(this).css("background-color", "rgba(49, 140, 207, 0.62)");
            var picName = "";
            var sourceText = $(this).text();
            for (var i = 1; i < sourceText.length - 1; i++)
                picName += sourceText[i];
            $("#HiddenInputPicName").attr("value", picName);
        });
    }
    changeTrigger = false;
}

function SwapBlocks() {
    //here is swap

    for (var i = 0; i < 15; i++) {
        var rand = Math.floor(Math.random() * (14 - 0 + 1)) + 0;
        //  alert(rand)
        var buffer = BlockInMassive[i];
        BlockInMassive[i] = BlockInMassive[rand];
        BlockInMassive[rand] = buffer;
    }
}

function BuildLastGame(Mas0,Mas1,Mas2,Mas3,Mas4,Mas5,Mas6,Mas7,Mas8,Mas9,Mas10,Mas11,Mas12,Mas13,Mas14,Mas15,StepsNumber,HavePicture,PictureName)
{
  
    var varii = 0;
  
    isGameContinued = true;
  
  var  StepsInGama = StepsNumber;
 
    FirstDisable = true;
  
    BlockInMassive[0] = Mas0; BlockInMassive[1] = Mas1; BlockInMassive[2] = Mas2;
    BlockInMassive[3] = Mas3; BlockInMassive[4] = Mas4; BlockInMassive[5] = Mas5;
    BlockInMassive[6] = Mas6; BlockInMassive[7] = Mas7; BlockInMassive[8] = Mas8;
    BlockInMassive[9] = Mas9; BlockInMassive[10] = Mas10; BlockInMassive[11] = Mas11;
    BlockInMassive[12] = Mas12; BlockInMassive[13] = Mas13; BlockInMassive[14] = Mas14;
    BlockInMassive[15] = Mas15;
   

    $(document).ready(function () {
        
        $('#NumberSteps').text(StepsInGama);
       
        if (HavePicture=="False") {
           
            BlockHavePicture = false;
            $("input[name='isPicThere']").attr("value", "0");
            $("#GameDesk").text("");
            for (var i = 0; i < 16; i++) {
                if (BlockInMassive[i] != "") {
                    if (BlockInMassive[i].length < 7)
                        varii = BlockInMassive[i][BlockInMassive[i].length - 1];
                    else
                        varii = BlockInMassive[i][BlockInMassive[i].length - 2] + BlockInMassive[i][BlockInMassive[i].length - 1];
                    $("#GameDesk").append('<div class="Block" id="' + BlockInMassive[i] + '">' + (parseInt(varii) + 1) + ' </div>');
                }
            }

            for (var i = 0; i < 16; i++) {
                if (BlockInMassive[i] != "") {
                    $("#" + BlockInMassive[i]).css("margin-left", Coord[i]);
                    $("#" + BlockInMassive[i]).css("margin-top", Coord[i + 16]);
                }
            }
        }
        else {
          
          
            BlockHavePicture = true;
            BlockPictureName = PictureName;
            $("input[name='bPicName']").attr("value", BlockPictureName);
            $("input[name='isPicThere']").attr("value", "1");
           // alert(PictureName+"-"+BlockHavePicture)
            var sourceP = (PicWay + PictureName+'.jpg');
            //alert("else working");
            $("#GameDesk").text("");
            for (var i = 0; i < 16; i++) {
                if (BlockInMassive[i] != "") {
                    if (BlockInMassive[i].length < 7)
                        varii = BlockInMassive[i][BlockInMassive[i].length - 1];
                    else
                        varii = BlockInMassive[i][BlockInMassive[i].length - 2] + BlockInMassive[i][BlockInMassive[i].length - 1];
                   // alert("source-"+sourceP+"-varii -"+varii+"-coord-"+Coord[varii]+"-2nd-"+Coord[(parseInt(varii) + 16)]);
                    $("#GameDesk").append('<div class="Block" style="background-image:url(' + sourceP + ');background-position:-' + Coord[varii] + ' -' + Coord[(parseInt(varii) + 16)] + ';   " id="' + BlockInMassive[i] + '"> </div>');
                  //  $("#GameDesk").append('<div class="Block" id="' + BlockInMassive[i] + '">' + varii + ' </div>');

                }
            }

            for (var i = 0; i < 16; i++) {
                if (BlockInMassive[i] != "") {
                    $("#" + BlockInMassive[i]).css("margin-left", Coord[i]);
                    $("#" + BlockInMassive[i]).css("margin-top", Coord[i + 16]);
                    $("#" + BlockInMassive[i]).css("width", "150px");
                    $("#" + BlockInMassive[i]).css("height", "150px");
                    $("#" + BlockInMassive[i]).css("padding", "0px");
                }
            }
        }
        

    });


    
}


function BuildBB() {
    var varii = 0;


    //alert('build bb run');
    isGameContinued = false;
    FirstDisable = true;
    $('#SaveGameStatus').attr('action', '/Home/SaveGameStatus');
    StepsInGama = 0;
    $('#NumberSteps').text(StepsInGama);
    //here starting add 1 Incomplete game to db and update values
    MassiveToNormalForm();
  //   SwapBlocks();
    $(document).ready(function () {


        // alert("build" + isBlockPicture);
        $("#GameDesk").text("");
        if (!BlockHavePicture) {
            for (var i = 0; i < 15; i++) {
                if (BlockInMassive[i].length < 7)
                    varii = BlockInMassive[i][BlockInMassive[i].length - 1];
                else
                    varii = BlockInMassive[i][BlockInMassive[i].length - 2] + BlockInMassive[i][BlockInMassive[i].length - 1];
                $("#GameDesk").append('<div class="Block" id="' + BlockInMassive[i] + '">' + (parseInt(varii)+1) + ' </div>');
            }
            for (var i = 0; i < 15; i++) {

                $("#" + BlockInMassive[i]).css("margin-left", Coord[i]);
                $("#" + BlockInMassive[i]).css("margin-top", Coord[i + 16]);



            }
            $("input[name='isPicThere']").attr("value","0");
        }
        else {
            var sourceP = (PicWay + BlockPictureName+'.jpg');
            // alert("-"+sourceP+"-");
            for (var i = 0; i < 15; i++) {
                if (BlockInMassive[i].length < 7)
                    varii = BlockInMassive[i][BlockInMassive[i].length - 1];
                else
                    varii = BlockInMassive[i][BlockInMassive[i].length - 2] + BlockInMassive[i][BlockInMassive[i].length - 1];
                //   
                $("#GameDesk").append('<div class="Block" style="background-image:url(' + sourceP + ');background-position:-' + Coord[varii] + ' -' + Coord[(parseInt(varii) + 16)] + ';   " id="' + BlockInMassive[i] + '"> </div>');
            }
            for (var i = 0; i < 15; i++) {

                $("#" + BlockInMassive[i]).css("margin-left", Coord[i]);
                $("#" + BlockInMassive[i]).css("margin-top", Coord[i + 16]);
                $("#" + BlockInMassive[i]).css("width", "150px");
                $("#" + BlockInMassive[i]).css("height", "150px");
                $("#" + BlockInMassive[i]).css("padding", "0px");


            }

            $("input[name='bPicName']").attr("value", BlockPictureName);
            $("input[name='isPicThere']").attr("value", "1");
        }






    });
    if (isFreezingMode) {
       // alert('interval setted');
     ZintervalID = setInterval(StartFury, 2000);
        }
}

function CheckMassiv() {
    var CheckVar = 0;



    for (var i = 0; i < 10; i++)
        if (BlockInMassive[i][BlockInMassive[i].length - 1] == i) {
            CheckVar++;

        }

    for (var i = 10; i < 15; i++)
        if (BlockInMassive[i][BlockInMassive[i].length - 2] + BlockInMassive[i][BlockInMassive[i].length - 1] == i) {
            CheckVar++;

        }

    if (CheckVar == 15) {
        if (isGameContinued)
            WinHandlerForContinuedGame();
        else
            WinHandlerForNormalGame();
      
    }
  


}

function WinHandlerForNormalGame() {
    isGameContinued = false;
    //Here starting add 1 win to Db and delete 1 incomplete game
    alert("Congratulations!");
    $("#SaveGameStatus").removeAttr("data-ajax-confirm");
    $('#SaveGameStatus').attr('action', '/Home/GameComplited');
    $('input[name="EndGameSubmit"]').click();
    $("#GameDesk").html('');
    $('#saveStatusz').attr('disabled', '');

    if (isFreezingMode) {
        clearInterval(ZintervalID);
       // alert('clearing interval');
        
        $('#saveStatusz').attr('disabled', 'disabled');
        FreezeModeCheck = false;
        isFreezingMode = false;
      
    }
  
    function EndGameIntervalClearer()
    {
        clearInterval(ZintervalID);
    }

}

function WinHandlerForContinuedGame() {
    isGameContinued = false;
    //Here starting add 1 win to Db and delete 1 incomplete game
    alert("Congratulations!");
    $("#SaveGameStatus").removeAttr("data-ajax-confirm");
    $('#SaveGameStatus').attr('action', '/Home/LastGameComplited');
    $('input[name="EndGameSubmit"]').click();
    $("#GameDesk").html('');
    $('#saveStatusz').attr('disabled', '');
  

}