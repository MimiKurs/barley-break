﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BB_MVC.Models.Entity;

namespace BB_MVC.Models
{
    public static class EntityService
    {
        
        private static BBUsersEntities context = new BBUsersEntities();

       
        public static bool Verificate(string Mail, string pass)
        { 
            try
            {
                var searchAndCheck = (from c in context.Users
                                      where c.Email == Mail
                                      select c
                    ).Single<Users>();
                

                if (searchAndCheck != null && searchAndCheck.Pass == pass)
                    return true;
                else
                    return false;
            }
            catch {
                return false;
            }

        }
        public static bool SaveGameStatus(string Email, int StepsNumber, string Mas0, string Mas1, string Mas2, string Mas3, string Mas4, string Mas5
            , string Mas6, string Mas7, string Mas8, string Mas9, string Mas10, string Mas11, string Mas12, string Mas13
            , string Mas14, string Mas15)
        {
            try
            {
                var searchAndCheck = (from c in context.GameStatus
                                      where c.Email == Email
                                      select c
                    ).Single<GameStatus>();
                searchAndCheck.Mas0 = Mas0;
                searchAndCheck.Mas1 = Mas1;
                searchAndCheck.Mas2 = Mas2;
                searchAndCheck.Mas3 = Mas3;
                searchAndCheck.Mas4 = Mas4;
                searchAndCheck.Mas5 = Mas5;
                searchAndCheck.Mas6 = Mas6;
                searchAndCheck.Mas7 = Mas7;
                searchAndCheck.Mas8 = Mas8;
                searchAndCheck.Mas9 = Mas9;
                searchAndCheck.Mas10 = Mas10;
                searchAndCheck.Mas11 = Mas11;
                searchAndCheck.Mas12 = Mas12;
                searchAndCheck.Mas13 = Mas13;
                searchAndCheck.Mas14 = Mas14;
                searchAndCheck.Mas15 = Mas15;
                searchAndCheck.isPictureThere = false;
                searchAndCheck.PictureName = "none";
                searchAndCheck.StepsInGame = StepsNumber;
                context.SaveChanges();
                var searchAndCheck2 = (from c in context.GameProgress
                                       where c.Email == Email
                                       select c
                    ).Single<GameProgress>();
                searchAndCheck2.isGameEnded = false;
                    return true;
            }
            catch
            {
                return false;
            }

        }

        public static bool AddPicToBlock(string Mail,bool HavePic, string PicName="")
        {
            try
            {
                var searchAndCheck = (from c in context.GameStatus
                                      where c.Email == Mail
                                      select c
                   ).Single<GameStatus>();

                if (HavePic)
                {
                    searchAndCheck.isPictureThere = true;
                    searchAndCheck.PictureName = PicName;
                    context.SaveChanges();
                    return true;
                }
                else
                {
                    searchAndCheck.isPictureThere = false;
                    context.SaveChanges();
                    return false;
                }
            }
            catch
            {
                throw new Exception("Error when add pic to block");
            }

        }

        public static bool StartNewGame(string Mail)
        {
            try
            {
                var searchAndCheck = (from c in context.GameProgress
                                      where c.Email == Mail
                                      select c
                    ).Single<GameProgress>();
                searchAndCheck.IncompleteGames++;
                context.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }

        }
        public static void GameFailed(string Mail)
        {
            try
            {
                var searchAndCheck = (from c in context.GameProgress
                                      where c.Email == Mail
                                      select c
                    ).Single<GameProgress>();
             
             
                searchAndCheck.isGameEnded = true;
                context.SaveChanges();
            }
            catch (Exception)
            {
                throw;
            }
        }


        public static void GetExpFromGame(string Mail, double zRecivedExp)
        {
            try
            {

                var searchAndCheck = (from c in context.Users
                                      where c.Email == Mail
                                      select c
                    ).Single<Users>();

             Int32  RecivedExp= Convert.ToInt32(zRecivedExp);

                if (searchAndCheck.CurrentExp + RecivedExp < searchAndCheck.MaxExp)
                    searchAndCheck.CurrentExp += RecivedExp;
                else
                {
                    while ((RecivedExp - (searchAndCheck.MaxExp - searchAndCheck.CurrentExp)) > 0)
                    {
                        RecivedExp = RecivedExp - (searchAndCheck.MaxExp - searchAndCheck.CurrentExp);
                        searchAndCheck.CurrentExp = 0;
                        searchAndCheck.Lvl++;
                        searchAndCheck.MaxExp = searchAndCheck.MaxExp + (searchAndCheck.MaxExp / 100 * 50);
                    }
                    
                        searchAndCheck.CurrentExp = RecivedExp;
                    
              
                    
                }
                 
                context.SaveChanges();
            }
            catch(Exception)
            {
               throw;
            }
        
        }

        public static void SortTopList(ref List<TopUsersInfo> list)
        {
            try
            {
                bool sort=true;
               
                while (sort)
                {   
                    sort = false;
                    for (int i = 0; i < list.Count-1; i++)
                    {
                        if (list[i].UserLvl < list[i + 1].UserLvl)
                        {
                            var buffL = list[i].UserLvl;
                            var buffN = list[i].UserName;
                            var buffA = list[i].UserAvgSteps;
                            var buffG = list[i].GamesPlayed;

                            list[i].UserLvl = list[i + 1].UserLvl;
                            list[i].UserName = list[i + 1].UserName;
                            list[i].UserAvgSteps = list[i + 1].UserAvgSteps;
                            list[i].GamesPlayed = list[i + 1].GamesPlayed;

                            list[i + 1].UserLvl = buffL;
                            list[i + 1].UserName = buffN;
                            list[i + 1].UserAvgSteps = buffA;
                            list[i + 1].GamesPlayed = buffG;

                            sort = true;
                        }
                        else if (list[i].UserLvl == list[i + 1].UserLvl)
                        {
                            if (list[i].GamesPlayed < list[i + 1].GamesPlayed)
                            {
                                var buffL = list[i].UserLvl;
                                var buffN = list[i].UserName;
                                var buffA = list[i].UserAvgSteps;
                                var buffG = list[i].GamesPlayed;

                                list[i].UserLvl = list[i + 1].UserLvl;
                                list[i].UserName = list[i + 1].UserName;
                                list[i].UserAvgSteps = list[i + 1].UserAvgSteps;
                                list[i].GamesPlayed = list[i + 1].GamesPlayed;

                                list[i + 1].UserLvl = buffL;
                                list[i + 1].UserName = buffN;
                                list[i + 1].UserAvgSteps = buffA;
                                list[i + 1].GamesPlayed = buffG;
                                sort = true;
                            }
                        }
                    }
                
                }



            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public static List<TopUsersInfo> GetTopUsers()
        {
            try
            {
                var searchAndCheck = (from c in context.Users
                                      select c
                    );
                 
                List<TopUsersInfo> Retlist = new List<TopUsersInfo>();
                foreach (var item in searchAndCheck)
                {
                    var searchAndCheck2 = (from c in context.GameProgress
                                           where c.Email==item.Email
                                           select c
                   ).Single<GameProgress>();

                    var Temp = new TopUsersInfo();
                    Temp.UserName = item.Name;
                    Temp.UserLvl = item.Lvl;
                    Temp.GamesPlayed = searchAndCheck2.CompleteGames + searchAndCheck2.IncompleteGames;
                    Temp.UserAvgSteps = searchAndCheck2.StepsInGames;
                    Retlist.Add(Temp);
                }
                return Retlist;

            }
            catch (Exception)
            {
                
                throw;
            }
        }


        public static bool LastGameComplited(string Mail, int StepsInGame)
        {
            try
            {
                var searchAndCheck = (from c in context.GameProgress
                                      where c.Email == Mail
                                      select c
                    ).Single<GameProgress>();
                if (searchAndCheck.IncompleteGames!=0)
                searchAndCheck.IncompleteGames--;
                searchAndCheck.CompleteGames++;
                searchAndCheck.StepsInGames += StepsInGame;
                searchAndCheck.isGameEnded = true;
                context.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }

        }

        public static bool GameComplited(string Mail,int StepsInGame)
        {
            try
            {
                var searchAndCheck = (from c in context.GameProgress
                                      where c.Email == Mail
                                      select c
                    ).Single<GameProgress>();
                searchAndCheck.IncompleteGames--;
                searchAndCheck.CompleteGames++;
                searchAndCheck.StepsInGames += StepsInGame;
                context.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }

        }

        public static UserStatsClass GetUserStatsFromEntity(string Email)
        {
            UserStatsClass UserStats = new UserStatsClass();
            var searchAndCheck = (from c in context.Users
                                  where c.Email == Email
                                  select c
             ).Single<Users>();

            UserStats.CurrentExp = searchAndCheck.CurrentExp;
            UserStats.MaxHp = searchAndCheck.MaxHp;
            UserStats.MaxExp = searchAndCheck.MaxExp;
            UserStats.Lvl = searchAndCheck.Lvl;
            UserStats.IncHpPoints = searchAndCheck.IncHpPoints;
            UserStats.IncExpPoints = searchAndCheck.IncExpPoints;
            UserStats.IncColdResistance = searchAndCheck.IncColdResPoints;
            UserStats.IncColdTimeout = searchAndCheck.IncColdTimeoutPoints;
                

            return UserStats;
        }

        public static UserLastGameDataClass GetLastGameFromEntity(string Email)
        {
            UserLastGameDataClass LastGame = new UserLastGameDataClass();
            var searchAndCheck = (from c in context.GameStatus
                                  where c.Email == Email
                                  select c
              ).Single<GameStatus>();

         

            LastGame.Mas0 = searchAndCheck.Mas0;
            LastGame.Mas1 = searchAndCheck.Mas1;
            LastGame.Mas2 = searchAndCheck.Mas2;
            LastGame.Mas3 = searchAndCheck.Mas3;
            LastGame.Mas4 = searchAndCheck.Mas4;
            LastGame.Mas5 = searchAndCheck.Mas5;
            LastGame.Mas6 = searchAndCheck.Mas6;
            LastGame.Mas7 = searchAndCheck.Mas7;
            LastGame.Mas8 = searchAndCheck.Mas8;
            LastGame.Mas9 = searchAndCheck.Mas9;
            LastGame.Mas10 = searchAndCheck.Mas10;
            LastGame.Mas11 = searchAndCheck.Mas11;
            LastGame.Mas12 = searchAndCheck.Mas12;
            LastGame.Mas13 = searchAndCheck.Mas13;
            LastGame.Mas14 = searchAndCheck.Mas14;
            LastGame.Mas15 = searchAndCheck.Mas15;
            LastGame.HavePicture = searchAndCheck.isPictureThere;
            LastGame.pictureName = searchAndCheck.PictureName ;
            LastGame.LastGameStepsNumber = searchAndCheck.StepsInGame;
            return LastGame;
        }


        public static UserDataClass GetUserFromEntity(string Email)
        {
            UserDataClass SettedUser = new UserDataClass();
            var searchAndCheck = (from c in context.GameProgress
                                  where c.Email == Email
                                  select c
              ).Single<GameProgress>();
            var searchAndCheck2 = (from c in context.Users
                                  where c.Email == Email
                                  select c
                   ).Single<Users>();
          
            SettedUser.Name = searchAndCheck2.Name;
            SettedUser.Surname = searchAndCheck2.Surname;
            SettedUser.CompleteGames=searchAndCheck.CompleteGames;
            SettedUser.IncompleteGames = searchAndCheck.IncompleteGames;
            SettedUser.StepsInGames = searchAndCheck.StepsInGames;
            SettedUser.isGameEnded = searchAndCheck.isGameEnded;
            SettedUser.Email = searchAndCheck.Email;
            SettedUser.HavePicture = searchAndCheck2.HavePicture;
            SettedUser.pictureName = searchAndCheck2.PPName;
            SettedUser.Tmargin = searchAndCheck2.Tmrg;
            SettedUser.Lmargin = searchAndCheck2.Lmrg;
            
            return SettedUser;
        }

        public static bool Register(string Email, string Password, string Name, string Surname)
        {
            try
            {
                Users CreateUser = new Users
                {
                    Name = Name,
                    Surname = Surname,
                    Email = Email,
                    Pass = Password,
                    HavePicture=true,
                    PPName="1111.jpg",
                    Lmrg="",
                    Tmrg="",
                    MaxHp=100,
                    MaxExp=500,
                    CurrentExp=0,
                    Lvl=1,
                    IncColdResPoints=0,
                    IncExpPoints=0,
                    IncColdTimeoutPoints=0,
                    IncHpPoints=0
                   };
                GameProgress Current =
                      new GameProgress
                      {
                          Email = Email,
                          CompleteGames = 0,
                          IncompleteGames = 0,
                          StepsInGames = 0,
                          isGameEnded = true
                      };
                GameStatus CurrentStatus =
                     new GameStatus
                     {
                         Email=Email,
                         StepsInGame=0,
                         Mas0="",
                         Mas1 = "",
                         Mas2 = "",
                         Mas3 = "",
                         Mas4 = "",
                         Mas5 = "",
                         Mas6 = "",
                         Mas7 = "",
                         Mas8 = "",
                         Mas9 = "",
                         Mas10 = "",
                         Mas11 = "",
                         Mas12 = "",
                         Mas13 = "",
                         Mas14 = "",
                         Mas15 = "",
                         isPictureThere=false,
                         PictureName=""
                     };
                CreateUser.GameProgress = Current;
                CreateUser.GameStatus = CurrentStatus;
                context.Users.Add(CreateUser);
                context.SaveChanges();
              
                return true;
            }
            catch
            {
                return false;
            }
           
        }

        public static bool AddPhotoToProfile(string Email, string Name,string Tmargin,string Lmargin)
        {
            try {
                var searchAndCheck = (from c in context.Users
                                      where c.Email == Email
                                      select c
                      ).Single<Users>();
                searchAndCheck.HavePicture = true;
                searchAndCheck.PPName = Name;
                searchAndCheck.Tmrg = Tmargin;
                searchAndCheck.Lmrg = Lmargin;
                context.SaveChanges();
                return true;
            
            }
            catch { return false; }


        }


        public static string isPassCorrect(string Pass)
        {
            string result="";
            if (Pass.Length > 5)
            {
                if (Pass.Length > 15)
                    result = "Password length must be little then 15 symbols.";
              
            }
            else
                result = "Password length must be more then 4 symbols.";
            
            return result;
        }

        public static string ChangeInfo(string Mail, string Vtype, string NewValue,string oldPass)
        {
            if (Vtype == "Name")
            {
                var searchAndCheck = (from c in context.Users
                                      where c.Email == Mail
                                      select c
                   ).Single<Users>();

                if (searchAndCheck != null)
                {
                    if (searchAndCheck.Name != NewValue)
                    {
                        searchAndCheck.Name = NewValue;
                        context.SaveChanges();
                        return "New name confirmed+";
                    }
                    else
                        return "Same value cant be changed-";
                }
                else
                    return "Cant change name. May be db error.-";
            }

            else if (Vtype == "Surname")
            {
                var searchAndCheck = (from c in context.Users
                                      where c.Email == Mail
                                      select c
                   ).Single<Users>();

                if (searchAndCheck != null)
                {
                    if (searchAndCheck.Surname != NewValue)
                    {
                        searchAndCheck.Surname = NewValue;
                        context.SaveChanges();
                        return "New surname confirmed+";
                    }
                    else
                        return "Same value cant be changed-";
                }
                else
                    return "Cant change surname. May be db error.-";
            }
        
          else if (Vtype == "Pass")
            {
                var searchAndCheck = (from c in context.Users
                                      where c.Email == Mail
                                      select c
                   ).Single<Users>();

                if (searchAndCheck != null)
                {
                    if (!string.IsNullOrEmpty(oldPass))
                    {
                        if(!(HashClass.GetHashFS(NewValue)==searchAndCheck.Pass))
                        {
                            if (HashClass.GetHashFS(oldPass) == searchAndCheck.Pass)
                            {
                                string fastVar = isPassCorrect(NewValue);
                                if (fastVar == "")
                                {
                                    string fastVarNew = isPassCorrect(NewValue);
                                    if (fastVarNew == "")
                                    {
                                        searchAndCheck.Pass = HashClass.GetHashFS(NewValue);
                                        context.SaveChanges();
                                        return "Password Changed+";
                                    }
                                    else
                                        return fastVarNew + "-";
                                }
                                else
                                    return fastVar + "-";
                            }
                            else
                                return "Old and Current password dont match-";
                        }
                        else
                            return "New pass cant be same like old pass-";
                    }
                    else
                        return "Old pass is empty-";
                }
                else
                    return "Cant find user-";
            }


            else if (Vtype == "Games")
            {
                var searchAndCheck = (from c in context.GameProgress
                                      where c.Email == Mail
                                      select c
                   ).Single<GameProgress>();

                if (searchAndCheck != null)
                {
                    searchAndCheck.CompleteGames = 0;
                    searchAndCheck.IncompleteGames = 0;
                    searchAndCheck.StepsInGames = 0;
                 
                    return "All statistics are cleared+";
                }
                else
                    return "Cant clear statistics, cant find user-";
            }
            else
                return "Dont know kommand-";
           
        }









    }
}