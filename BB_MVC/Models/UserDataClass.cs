﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BB_MVC.Models
{
    public class UserDataClass
    {
        public string Name;
        public string Surname;
        public string Email;
        public int CompleteGames;
        public int IncompleteGames;
        public int StepsInGames;
        public bool isGameEnded;
        public bool HavePicture;
        public string pictureName;
        public string Tmargin;
        public string Lmargin;

                
    }
   
    public class UserStatsClass
    {
        public int MaxHp;
        public int MaxExp;
        public int CurrentExp;
        public int Lvl;
        public int IncHpPoints;
        public int IncExpPoints;
        public int IncColdResistance;
        public int IncColdTimeout;
    }

     public class UserLastGameDataClass
    {
                 public string Mas0;
                 public string Mas1;
                 public string Mas2;
                 public string Mas3;
                 public string Mas4;
                 public string Mas5;
                 public string Mas6;
                 public string Mas7;
                 public string Mas8;
                 public string Mas9;
                 public string Mas10;
                 public string Mas11;
                 public string Mas12;
                 public string Mas13;
                 public string Mas14;
                 public string Mas15;
                 public bool HavePicture;
                 public string pictureName;
                 public int LastGameStepsNumber;
    }
     public class TopUsersInfo
     {
         public string UserName;
         public int UserLvl;
         public int UserAvgSteps;
         public int GamesPlayed;
     }
}