﻿using BB_MVC.Models;
using Resources;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BB_MVC.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            
            return View();



        }
        public string SaveGameStatus(int StepsNumber, string Mas0, string Mas1, string Mas2, string Mas3, string Mas4, string Mas5
            , string Mas6, string Mas7, string Mas8, string Mas9, string Mas10, string Mas11, string Mas12, string Mas13
            , string Mas14, string Mas15,string bPicName,string isPicThere)
        {
            bool ConvIsPicThere;
            if (isPicThere == "1")
                ConvIsPicThere = true;
            else
                ConvIsPicThere = false;

            if (EntityService.SaveGameStatus(Session["CurrentUserMail"].ToString(), StepsNumber, Mas0, Mas1, Mas2, Mas3, Mas4, Mas5, Mas6,
                Mas7, Mas8, Mas9, Mas10, Mas11, Mas12, Mas13, Mas14, Mas15))
            {
               var res1= EntityService.AddPicToBlock(Convert.ToString(Session["CurrentUserMail"]), ConvIsPicThere, bPicName);
                return "<script> ProcessedToCabinet(true,'Saving game status was succesfull.') </script>"; }
            else
                return "<script> ProcessedToCabinet(false,'Saving game status was not succesfull.Mb loose connection to server?') </script>";

        }

        public string StartNewGame()
        {
            if (Session["CurrentUserMail"] != null)
            {
                if (EntityService.StartNewGame(Session["CurrentUserMail"].ToString()))
                {
                    var CurrentUser = EntityService.GetUserFromEntity(Session["CurrentUserMail"].ToString());
                    var CurrentStats = EntityService.GetUserStatsFromEntity(Session["CurrentUserMail"].ToString());
                    double a = 0;
                    try
                    {
                        a =Convert.ToInt16( CurrentUser.StepsInGames / CurrentUser.CompleteGames);
                    }
                    catch
                    { }
                    string RestoreHp = "GFreezeVar=0; DegradeHpBarTo(0);";
                    //string NewIfo = "$('#StatisticsInfo').html('Games played: " + (CurrentUser.CompleteGames + CurrentUser.IncompleteGames) + "  <br>Complete Games:" + CurrentUser.CompleteGames + " <br> Incomplete Games : " + CurrentUser.IncompleteGames + "  <br> Steps In Game: " + CurrentUser.StepsInGames + " <br> Average Steps: " + a + "  <br>')";
                    string NewIfo = String.Format("$('#StatisticsInfo').html('Level:<span id=\"SpanLevelChange\">{5} </span><br/>Games played: {0} <br>Complete Games: {1} <br> Incomplete Games : {2}  <br> Steps In Game: {3}  <br> Average Steps: {4} <br>')", (CurrentUser.CompleteGames + CurrentUser.IncompleteGames), CurrentUser.CompleteGames, CurrentUser.IncompleteGames, CurrentUser.StepsInGames, a, CurrentStats.Lvl);
                    return String.Format("<script> ProcessedToCabinet(true,'New game was started');BuildBB(); {1} blockMoveControl();{0}  </script>", NewIfo, RestoreHp);
                }
                else
                return "<script> ProcessedToCabinet(false,'New game was not started') </script>";
            }
            else
                return "<script> ProcessedToCabinet(false,'New game was not started') </script>";
        }

        public string StartNewLastGame()
        {
            try
            {

                var CurrentUser = EntityService.GetUserFromEntity(Session["CurrentUserMail"].ToString());
                var GEtData = EntityService.GetLastGameFromEntity(Session["CurrentUserMail"].ToString());
                var CurrentStats = EntityService.GetUserStatsFromEntity(Session["CurrentUserMail"].ToString());
                double a = 0;
                try
                {
                    a = Convert.ToInt16(CurrentUser.StepsInGames / CurrentUser.CompleteGames);
                }
                catch
                { }
                string NewIfo = String.Format("$('#StatisticsInfo').html('Level:<span id=\"SpanLevelChange\">{5} </span><br/>Games played: {0} <br>Complete Games: {1} <br> Incomplete Games : {2}  <br> Steps In Game: {3}  <br> Average Steps: {4} <br>')", (CurrentUser.CompleteGames + CurrentUser.IncompleteGames), CurrentUser.CompleteGames, CurrentUser.IncompleteGames, CurrentUser.StepsInGames, a, CurrentStats.Lvl);
                string buildLastGamestring =String.Format("BuildLastGame('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}','{16}','{17}','{18}');",GEtData.Mas0,GEtData.Mas1,GEtData.Mas2,GEtData.Mas3,GEtData.Mas4,GEtData.Mas5,GEtData.Mas6,GEtData.Mas7,GEtData.Mas8,GEtData.Mas9,GEtData.Mas10,GEtData.Mas11,GEtData.Mas12,GEtData.Mas13,GEtData.Mas14,GEtData.Mas15, GEtData.LastGameStepsNumber,GEtData.HavePicture,GEtData.pictureName);
                
                return String.Format("<script> ProcessedToCabinet(true,'Last game was started');{0} blockMoveControl(); {1} </script>",buildLastGamestring,NewIfo);
            }
            catch
            {
                return "<script> ProcessedToCabinet(false,'New game was not started') </script>";
            }
        }

        public string GameFailed()
        {
            EntityService.GameFailed(Session["CurrentUserMail"].ToString());
            // NEED COMPLETE LOCALIZATION!!!
            return string.Format("<script> ProcessedToCabinet(true,'{0}')</script> ", Messages.GameOver);
          //  return string.Format("<script> ProcessedToCabinet(true,'Game over try again')</script> ");
        }

        public string GameComplited(int StepsNumber, string isPicThere)
        {
            if (EntityService.GameComplited(Session["CurrentUserMail"].ToString(), StepsNumber))
            {
                double a = 0;
                
                var CurrentStats = EntityService.GetUserStatsFromEntity(Session["CurrentUserMail"].ToString());
                var CurrentUser = EntityService.GetUserFromEntity(Session["CurrentUserMail"].ToString());
                int oldLevl = CurrentStats.Lvl;
                Random rnd = new Random();
                
                double RecivedExp = 200 + rnd.Next(Convert.ToInt16(10 * ((CurrentStats.IncExpPoints / 7) + 1)), Convert.ToInt16(100 * ((CurrentStats.IncExpPoints / 7) + 1))) + (125*CurrentStats.Lvl);
                if (isPicThere == "1")
                    RecivedExp = RecivedExp + (RecivedExp / 100 * 50);
                try
                {
                    EntityService.GetExpFromGame(Session["CurrentUserMail"].ToString(), RecivedExp); //Write into entity module which check exp + set exp if level up and other
                }
                catch (Exception)
                {
                  throw;
                }
                
                try
                {
                    a = Convert.ToInt16(CurrentUser.StepsInGames / CurrentUser.CompleteGames);
                }
                catch
                { }
                CurrentStats = EntityService.GetUserStatsFromEntity(Session["CurrentUserMail"].ToString());
                string IncHpExpBars = "";
                if (oldLevl != CurrentStats.Lvl)
                {
                    IncHpExpBars = String.Format("DegradeExpBarTo(true,{0});",CurrentStats.CurrentExp);
                }
                else
                {
                    IncHpExpBars = String.Format("DegradeExpBarTo(false,{0});", CurrentStats.CurrentExp);
                }
                string NewIfo = String.Format("$('#StatisticsInfo').html('Level:<span id=\"SpanLevelChange\"> {5}</span> <br/>Games played: {0} <br>Complete Games: {1} <br> Incomplete Games : {2}  <br> Steps In Game: {3}  <br> Average Steps: {4} <br>');", (CurrentUser.CompleteGames + CurrentUser.IncompleteGames), CurrentUser.CompleteGames, CurrentUser.IncompleteGames, CurrentUser.StepsInGames, a, CurrentStats.Lvl);
                return String.Format("<script> ProcessedToCabinet(true,'Game results was saved!');  {0} {1} </script>", NewIfo, IncHpExpBars);
            }
            else
                return "<script> ProcessedToCabinet(false,'Game results was not saved!!!!') </script>";
        }
        public string LastGameComplited(int StepsNumber)
        {
            if (EntityService.LastGameComplited(Session["CurrentUserMail"].ToString(), StepsNumber))
            {
                var CurrentStats = EntityService.GetUserStatsFromEntity(Session["CurrentUserMail"].ToString());
                var CurrentUser = EntityService.GetUserFromEntity(Session["CurrentUserMail"].ToString());
                var CurrentStatusGame =EntityService.GetLastGameFromEntity(Session["CurrentUserMail"].ToString());
                double a = 0;
                int oldLevl = CurrentStats.Lvl;
                Random rnd = new Random();
                double RecivedExp = 200 + rnd.Next(Convert.ToInt16(10 * ((CurrentStats.IncExpPoints / 7) + 1)), Convert.ToInt16(100 * ((CurrentStats.IncExpPoints / 7) + 1))) + (125 * CurrentStats.Lvl);
                if (CurrentStatusGame.HavePicture)
                    RecivedExp = RecivedExp + (RecivedExp / 100 * 50);
                try
                {
                    EntityService.GetExpFromGame(Session["CurrentUserMail"].ToString(), RecivedExp);
                }
                catch (Exception)
                {

                    throw;
                }
                

                try
                {
                    a = Convert.ToInt16(CurrentUser.StepsInGames / CurrentUser.CompleteGames);
                }
                catch
                { }

                CurrentStats = EntityService.GetUserStatsFromEntity(Session["CurrentUserMail"].ToString());
                string IncHpExpBars = "";
                if (oldLevl != CurrentStats.Lvl)
                {
                    IncHpExpBars = String.Format("DegradeExpBarTo(true,{0});", CurrentStats.CurrentExp);
                }
                else
                {
                    IncHpExpBars = String.Format("DegradeExpBarTo(false,{0});", CurrentStats.CurrentExp);
                }
                string NewIfo = String.Format("$('#StatisticsInfo').html('Level:<span id=\"SpanLevelChange\">{5} </span><br/>Games played: {0} <br>Complete Games: {1} <br> Incomplete Games : {2}  <br> Steps In Game: {3}  <br> Average Steps: {4} <br>');", (CurrentUser.CompleteGames + CurrentUser.IncompleteGames), CurrentUser.CompleteGames, CurrentUser.IncompleteGames, CurrentUser.StepsInGames, a, CurrentStats.Lvl);
                return String.Format("<script> ProcessedToCabinet(true,'Game results was saved!'); {0} {1} </script>", NewIfo,IncHpExpBars);
            }
            else
                return "<script> ProcessedToCabinet(false,'Game results was not saved!!!!') </script>";
        }

        public string LogOn(string Email, string Password, bool Remember = false)
        {
            if (Email.Length < 5 || Password.Length < 5)
            {
                return "<script> ProcessedToCabinet(false,'Error: Field is not filled properly. (Fields must be more than 5 chars)') </script>";
            }
            else if (Email.IndexOf('@') == -1 || Email.IndexOf('.') == -1 || Email.IndexOf('@') == 0 || Email.IndexOf('@') > Email.IndexOf('.'))
            {
                return "<script> ProcessedToCabinet(false,'Error: Field is not filled properly. (Email Not Correct)') </script>";
            }
            else
            {

                if (EntityService.Verificate(Email, HashClass.GetHashFS(Password)))
                {
                    var CurrentUser = EntityService.GetUserFromEntity(Email);
                    var CurrentStats = EntityService.GetUserStatsFromEntity(Email);
                    ViewBag.CurrentUser = CurrentUser;
                    if (Remember)
                    {

                        HttpCookie aCookie = new HttpCookie("usrEmail");
                        aCookie.Value = Email;
                        aCookie.Expires = DateTime.Now.AddDays(1);
                        Response.Cookies.Add(aCookie);

                        HttpCookie bCookie = new HttpCookie("usrPass");
                        bCookie.Value = Password;
                        bCookie.Expires = DateTime.Now.AddDays(1);
                        Response.Cookies.Add(bCookie);
                    }
                    Session["CurrentUserMail"] = Email;
                    Session["CurrentUserPass"] = HashClass.GetHashFS(Password);
                    if (CurrentUser.isGameEnded)
                    {
                        string a = String.Format("<script> ProcessedToCabinet(true);TransformToCabinet(true,{9},'{0}','{1}','{2}',{3},{4},{5},{6},{7},{8}); ", CurrentUser.Name, CurrentUser.Surname, CurrentUser.Email, CurrentUser.CompleteGames, CurrentUser.IncompleteGames,Convert.ToInt16(CurrentUser.StepsInGames),CurrentStats.MaxHp,CurrentStats.MaxExp,CurrentStats.CurrentExp,CurrentStats.Lvl);
                       if(CurrentUser.HavePicture)
                       {
                           string photoCheck = String.Format(" SetAvatarka('{0}','{1}','{2}'); ", CurrentUser.pictureName, CurrentUser.Tmargin, CurrentUser.Lmargin);
                           return String.Format("{0} {1} </script>",a,photoCheck);
                       }
                       else
                       return String.Format("{0} </script>",a);
                    }
                    else
                    {

                        string a = String.Format("<script> buildLastGameForm(); ProcessedToCabinet(true);TransformToCabinet(true,{9},'{0}','{1}','{2}',{3},{4},{5},{6},{7},{8}); ", CurrentUser.Name, CurrentUser.Surname, CurrentUser.Email, CurrentUser.CompleteGames, CurrentUser.IncompleteGames, Convert.ToInt16(CurrentUser.StepsInGames), CurrentStats.MaxHp, CurrentStats.MaxExp, CurrentStats.CurrentExp, CurrentStats.Lvl);
                        if (CurrentUser.HavePicture)
                        {
                            string photoCheck = String.Format(" SetAvatarka('{0}','{1}','{2}'); ", CurrentUser.pictureName, CurrentUser.Tmargin, CurrentUser.Lmargin);
                            return String.Format("{0} {1} </script>", a, photoCheck);
                        }
                        else
                            return String.Format("{0} </script>", a);
                    }

                }
                else
                {
                    return "<script> ProcessedToCabinet(false) </script>";
                }
            }

        }

     /*   public string AddPicToBLock(bool isPict, string picName = "")
        {
            if (isPict)
            {

                return "";
            }
            else
            {

                return "";
            }


        }
      */

        public string ReloadAvatar()
        {
            var CurrentUser = EntityService.GetUserFromEntity(Convert.ToString( Session["CurrentUserMail"]));
            if (CurrentUser.HavePicture)
            {
                string photoCheck = String.Format("<script> SetAvatarka('{0}','{1}','{2}'); ProcessedToCabinet(true,'New Photo was accepted');krestik();  </script>", CurrentUser.pictureName, CurrentUser.Tmargin, CurrentUser.Lmargin);
                return  photoCheck;
            }
            else
                return "<script>ProcessedToCabinet(false,'New Photo was not accepted');krestik(); </script>";

        }



        public string Registrate(string Email, string Password, string Password2, string Name, string Surname)
        {
            if (Email.Length < 5 || Password.Length < 6 || Name.Length < 1 || Surname.Length < 1 || Password != Password2)
            {
                string ResultedString = "Next data is not correct <br/>";
                if (Password != Password2)
                    ResultedString += "<span style='color:red;'> Password1 not like Password2 </span> <br/>";
                if (Email.Length < 5)
                    ResultedString += "<span style='color:red;'>Email must be more then 4 symbols </span> <br/>";
                if (Password.Length < 5)
                    ResultedString += "<span style='color:red;'> Password must be more then 4 symbols </span> <br/>";
                if (Name.Length < 1)
                    ResultedString += "<span style='color:red;'> Name must be more then 1 symbol </span> <br/>";
                if (Surname.Length < 1)
                    ResultedString += "<span style='color:red;'>Surname must be more then 1-                                                     symbols</span> <br/>";
                string res = String.Format("<script> ProcessedToCabinet(false,\" {0} \") </script>", ResultedString);
                return res;
            }
            else if (Email.IndexOf('@') == -1 || Email.IndexOf('.') == -1 || Email.IndexOf('@') == 0 || Email.IndexOf('@') > Email.IndexOf('.'))
            {
                return "<script> ProcessedToCabinet(false,'Error: Field is not filled properly. (Email Not Correct)') </script>";
            }
            else
            {



                if (EntityService.Register(Email, HashClass.GetHashFS(Password), Name, Surname))
                {

                    return "<script> ProcessedToCabinet(true,'Register was success.Please Log in.') </script>";
                }
                else
                {
                    return "<script> ProcessedToCabinet(false,'Register was failed. Error with updating data.<span style=\"color:Red;\"> Email can be picked by other user or data was not correct.</span> Please check Your data and try again.') </script>";
                }
            }
        }

        /*
           public string _downloadFile(IEnumerable<HttpPostedFileBase>  fileUpload,string Tmargin,string Lmargin)
           {
               try
               {
                   foreach (var file in fileUpload)
                   {
                       string newFilename =Convert.ToString( Session["CurrentUserMail"]);
                       if (file == null) continue;
                       string path = AppDomain.CurrentDomain.BaseDirectory + "ProfilePhotos/";
                       string filename = Path.GetFileName(file.FileName);

                       for (int i = filename.LastIndexOf("."); i < filename.Length; i++)
                           newFilename += filename[i];

                       if (filename != null) file.SaveAs(Path.Combine(path, newFilename));
                      // EntityService.AddPhotoToProfile(Convert.ToString(Session["CurrentUserMail"]),newFilename);
                       bool res = EntityService.AddPhotoToProfile(Convert.ToString(Session["CurrentUserMail"]), "1111.jpg",Tmargin, Lmargin);
                   }

                   return "<script> ProcessedToCabinet(true,'Photo download was success.'); krestik(); </script>";
               }
               catch
               {
                   return "<script> ProcessedToCabinet(false,'Photo download was failed.')";

               }

           }
     

           public ActionResult downloadFile(string format,string Tmargin, string Lmargin)
           {
               try
               {//Convert.ToString(Session["CurrentUserMail"])


                   bool res = EntityService.AddPhotoToProfile(Convert.ToString(Session["CurrentUserMail"]), format, Tmargin, Lmargin);


                       return RedirectToAction("ReloadAvatar");
                    
                  
               }
               catch(Exception ex)
               {
                   throw new Exception("Error while download file");

               }

           } */
        public ActionResult downloadFile( string Tmargin, string Lmargin)
        {
            try
            {//Convert.ToString(Session["CurrentUserMail"])


                bool res = EntityService.AddPhotoToProfile(Convert.ToString(Session["CurrentUserMail"]),"1111.jpg", Tmargin, Lmargin);


                return RedirectToAction("ReloadAvatar");


            }
            catch (Exception)
            {
                throw new Exception("Error while download file");

            }

        }

        public string RequestTopUsers()
        {
            try
            {
                var TopUsers= EntityService.GetTopUsers();
                if (TopUsers.Count > 1)
                    EntityService.SortTopList(ref TopUsers);

                //build bable of users here
                //return String.Format("<script>ProcessedToCabinet(true,'') </script>");
                string Table = "<table id='topResultsTable'><tr> <th> Level </th> <th> Name </th> <th> Games played </th> <th> Average Steps </th></tr>";
                foreach (var item in TopUsers)
                {
                    Table += "<tr> <td class='topResultsTableLvl'>" + item.UserLvl + "</td>  <td>" + item.UserName + "</td>  <td>" + item.GamesPlayed + "</td>  <td>" + item.UserAvgSteps + "</td> </tr>";

                }
                Table += "</table>";
                string Block = String.Format("$('#GameDesk').prepend(\"<div id='Bigblocku' style=' background-color:white;'><span id='krestik' onclick='krestik()'>&nbsp; x &nbsp;</span> {0} <div>\"); ", Table);
                string SetBlocks = "$('.topResultsTableLvl:lt(3)').css({\"padding-left\":\"7px\",\"padding-top\":\"3px\",\"margin-left\":\"10px\",\"font-size\":\"20px\",\"font-weight\":\"600\"});";
                return String.Format("<script> {0} {1} </script>", Block,SetBlocks);
                
            }
            catch (Exception ex)
            {
                
                return String.Format( "<script>ProcessedToCabinet(false,'{0}') </script>",ex.Message);
            }
        }


        public string DownloadPhotoToServer(IEnumerable<HttpPostedFileBase> fileUpload)
        {
            try
            {
                foreach (var file in fileUpload)
                {
                    string newFilename = Convert.ToString(Session["CurrentUserMail"]);
                    if (file == null) continue;
                    string path = AppDomain.CurrentDomain.BaseDirectory + "ProfilePhotos/";
                    string filename = Path.GetFileName(file.FileName);

                    for (int i = filename.LastIndexOf("."); i < filename.Length; i++)
                        newFilename += filename[i];

                    if (filename != null)
                        file.SaveAs(Path.Combine(path, newFilename));
                    
                    // EntityService.AddPhotoToProfile(Convert.ToString(Session["CurrentUserMail"]),newFilename);
                    // bool res = EntityService.AddPhotoToProfile(Convert.ToString(Session["CurrentUserMail"]), "1111.jpg",Tmargin, Lmargin);




                }
                return "<script>ProcessedToCabinet(true,'file downloaded') </script>";
            }
            catch { return "<script>ProcessedToCabinet(false,'file not downloaded') </script>"; }

        }



        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public string ChangeValue(string variable, string NewValue, string oldPass="")
        {
            if (!string.IsNullOrEmpty(variable))
            {
                var CurrentStats = EntityService.GetUserStatsFromEntity(Session["CurrentUserMail"].ToString());
               string Result= EntityService.ChangeInfo(Session["CurrentUserMail"].ToString(), variable, NewValue, oldPass);
               string res2="";
               for (int i = 0; i < Result.Length - 1; i++)
                   res2 += Result[i];
               if (Convert.ToString(Result[Result.Length - 1]) == "+")
               {
                   string UpdateValues = "";
                   if (variable == "Name")
                   {
                       var CurrentUserUpdate = EntityService.GetUserFromEntity(Session["CurrentUserMail"].ToString());
                       UpdateValues = string.Format("$('#PersonalBaseInfo').html('<b>Name: </b><br> {0} <br><b> Surname: </b><br> {1} <br><b>Email: </b><br> {2} <br>');", CurrentUserUpdate.Name, CurrentUserUpdate.Surname, CurrentUserUpdate.Email);
                   }
                   else if (variable == "Surname")
                   {
                       var CurrentUserUpdate = EntityService.GetUserFromEntity(Session["CurrentUserMail"].ToString());
                       UpdateValues = string.Format("$('#PersonalBaseInfo').html('<b>Name: </b><br>{0}<br><b> Surname: </b><br>{1}<br><b>Email: </b><br>{2}<br>');",CurrentUserUpdate.Name,CurrentUserUpdate.Surname,CurrentUserUpdate.Email);
                   }    
                   else if (variable == "Games")
                   {
                       var CurrentUserUpdate = EntityService.GetUserFromEntity(Session["CurrentUserMail"].ToString());
                       double a = 0;
                       if (CurrentUserUpdate.CompleteGames != 0)
                       {
                           a = CurrentUserUpdate.StepsInGames / CurrentUserUpdate.CompleteGames;
                       }
                       UpdateValues = String.Format("$('#StatisticsInfo').html('Level:<span id=\"SpanLevelChange\">{5} </span><br/>Games played: {0} <br>Complete Games: {1} <br> Incomplete Games : {2}  <br> Steps In Game: {3}  <br> Average Steps: {4} <br>')", (CurrentUserUpdate.CompleteGames + CurrentUserUpdate.IncompleteGames), CurrentUserUpdate.CompleteGames, CurrentUserUpdate.IncompleteGames, CurrentUserUpdate.StepsInGames, a, CurrentStats.Lvl);
                   }
                   return String.Format("<script>krestik(); ProcessedToCabinet(true,'{0}');{1}", res2, UpdateValues);
               }
               else
                   return String.Format("<script> ProcessedToCabinet(false,'{0}')", res2);
                  
            }
            else
                return "<script> ProcessedToCabinet(false,'cant change value')";

        }


    }
}
