USE [BBUsers]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 09/08/2013 19:53:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[Name] [nvarchar](20) NOT NULL,
	[Surname] [nvarchar](20) NOT NULL,
	[Email] [nvarchar](20) NOT NULL,
	[Pass] [nvarchar](20) NOT NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[Email] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[GameStatus]    Script Date: 09/08/2013 19:53:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GameStatus](
	[Email] [nvarchar](20) NOT NULL,
	[StepsInGame] [int] NOT NULL,
	[Mas0] [nvarchar](20) NOT NULL,
	[Mas1] [nvarchar](20) NOT NULL,
	[Mas2] [nvarchar](20) NOT NULL,
	[Mas3] [nvarchar](20) NOT NULL,
	[Mas4] [nvarchar](20) NOT NULL,
	[Mas5] [nvarchar](20) NOT NULL,
	[Mas6] [nvarchar](20) NOT NULL,
	[Mas7] [nvarchar](20) NOT NULL,
	[Mas8] [nvarchar](20) NOT NULL,
	[Mas9] [nvarchar](20) NOT NULL,
	[Mas10] [nvarchar](20) NOT NULL,
	[Mas11] [nvarchar](20) NOT NULL,
	[Mas12] [nvarchar](20) NOT NULL,
	[Mas13] [nvarchar](20) NOT NULL,
	[Mas14] [nvarchar](20) NOT NULL,
	[Mas15] [nvarchar](20) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Email] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[GameProgress]    Script Date: 09/08/2013 19:53:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GameProgress](
	[Email] [nvarchar](20) NOT NULL,
	[CompleteGames] [int] NOT NULL,
	[IncompleteGames] [int] NOT NULL,
	[StepsInGames] [int] NOT NULL,
	[isGameEnded] [bit] NOT NULL,
 CONSTRAINT [PK_GameProgress] PRIMARY KEY CLUSTERED 
(
	[Email] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  ForeignKey [FK__GameProgr__Email__0BC6C43E]    Script Date: 09/08/2013 19:53:23 ******/
ALTER TABLE [dbo].[GameProgress]  WITH CHECK ADD FOREIGN KEY([Email])
REFERENCES [dbo].[Users] ([Email])
GO
/****** Object:  ForeignKey [FK__GameStatu__Email__1273C1CD]    Script Date: 09/08/2013 19:53:23 ******/
ALTER TABLE [dbo].[GameStatus]  WITH CHECK ADD FOREIGN KEY([Email])
REFERENCES [dbo].[Users] ([Email])
GO
